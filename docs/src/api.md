# API Docs

## Public
!!! note

    See the [Algorithms and Parallelism](@ref algorithms) section for the doc strings of
    particular algorithms.

Only `shapley` is exported.

```@docs
shapley
Shapley.summary
```

## All
```@autodocs
Modules = [Shapley]
```
