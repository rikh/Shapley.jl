using Shapley
using Documenter

makedocs(;
    modules=[Shapley],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Shapley.jl/blob/{commit}{path}#{line}",
    sitename="Shapley.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Shapley.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Algorithms" => "algorithms.md",
        "API" => "api.md",
        "Bibliography" => "bibliography.md",
    ],
)

deploydocs(;
    repo="gitlab.com/ExpandingMan/Shapley.jl",
)
