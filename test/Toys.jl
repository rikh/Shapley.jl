module Toys

using Random, LinearAlgebra, Tables


function linear(a::AbstractVector{<:Number}, b::Number)
    f(X::AbstractArray) = [a⋅view(X, i, :) for i ∈ 1:size(X,1)] .+ b
    f(X) = f(Tables.matrix(X))
    f
end

function quadratic(A::AbstractMatrix{<:Number}, a::AbstractVector{<:Number}, b::Number)
    function f(X::AbstractArray)
        o = Vector{promote_type(eltype(X), eltype(A))}(undef, size(X,1))
        for i ∈ 1:length(o)
            x = view(X, i, :)
            o[i] = x' * A * x + a⋅x + b
        end
        o
    end
    f(X) = f(Tables.matrix(X))
    f
end

function data(f::Function, n::Integer, m::Integer, σ::Real; rng::AbstractRNG=Random.GLOBAL_RNG)
    X = rand(rng, n, m)
    y = f(X) .+ σ*randn(n)
    Tables.table(X), y
end

end
