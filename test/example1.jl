using Shapley, MLJ, ComputationalResources, DataFrames
import RDatasets

using Shapley: MonteCarlo

using MLJDecisionTreeInterface: RandomForestRegressor


boston = RDatasets.dataset("MASS", "Boston")

y, X = unpack(boston, ==(:MedV), col -> true)

m = machine(RandomForestRegressor(), X, y)

fit!(m)

df = DataFrame(Shapley.summary(X -> predict(m, X), MonteCarlo(CPUThreads(), 2048), X))

