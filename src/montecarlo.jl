
"""
    MonteCarlo{P<:AbstractResource,R<:AbstractRNG}

A `Shapley.Algorithm` object for describing the computation of Shapley values via the monte carlo algorithm introduced
by [Strumbelj, Kononenko 2014](@ref bibliography).

The `res` argument in the constructors specifies a computational resource for executing the computation.  Currently
available options are `CPU1()` (single threaded) and `CPUThreads()` (all threads available to the Julia process).
These are provided by the [ComputationalResources.jl](https://github.com/timholy/ComputationalResources.jl) package.

## Constructors
- `MonteCarlo(res, M, rng=Random.GLOBAL_RNG)`
- `MonteCarlo(M, rng=Random.GLOBAL_RNG)`

## Arguments
- `res`: An `AbstractResource` computational resource.  Pass `CPUThreads()` to run multi-threaded.
- `M`: The total number of iterations (samples).
- `rng`: A random number generator object to use.
"""
struct MonteCarlo{P,R<:AbstractRNG} <: Algorithm{P}
    resource::P
    M::Int
    rng::R

    function MonteCarlo(res::AbstractResource, M::Integer, rng::AbstractRNG=Random.GLOBAL_RNG)
        new{typeof(res),typeof(rng)}(res, M, rng)
    end
end
MonteCarlo(M::Integer, rng::AbstractRNG=Random.GLOBAL_RNG) = MonteCarlo(CPU1(), M, rng)

isindependent(m::MonteCarlo) = true
supports_classification(m::MonteCarlo) = true

"""
    imputeshuffled!(rng, j, ξt, Xt, Zt)

Impute shuffled data into an empty dataset created for the Shapley monte carlo algorithm.
"""
function imputeshuffled!(rng::AbstractRNG, j::Integer, ξt::Columns, Xt::Columns, Zt::Columns)
    n = Tables.rowcount(ξt)
    p = rand(rng, 1:Tables.rowcount(Xt), n)
    for (i, c) ∈ enumerate(ξt)
        i == j && continue  # this column will be handled after
        ξt[i] .= ifelse.(rand(rng, Bool, n), Xt[i][p], Zt[i])
    end
    p
end

"""
    shuffledata(rng, X, j, Z)

Return shuffled instances of `Z` as needed by the Shapley monte carlo algorithm.
"""
function shuffledata(rng::AbstractRNG, X, j::Integer, Z)
    ξ = similartable(Z)
    ξt, Xt, Zt = compatibletable.((ξ, X, Z))
    p = imputeshuffled!(rng, j, ξt, Xt, Zt)
    ξt[j] .= Zt[j]
    ζ = ntcopy(ξ)
    compatibletable(ζ)[j] .= Xt[j][p]
    ξ, ζ
end

"""
    montecarlo(predict, m, X, j, Z)

A single iteration of the Shapley monte carlo algorithm.
"""
function montecarlo(predict::Function, m::MonteCarlo, X, j::Integer, Z)
    ξ, ζ = shuffledata(m.rng, X, j, Z)
    predict(ξ) ⊟ predict(ζ)
end

function shapley(predict::Function, m::MonteCarlo, X, j::FeatureIndex, Z=X)
    op = (a, b) -> tableop(+, a, b)
    o = reduce(op, _map(m.resource)(i -> montecarlo(predict, m, X, featureindex(X, j), Z), 1:m.M))
    tableop(/, o, m.M)
end

